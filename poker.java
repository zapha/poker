import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

/**
 * poker
 */
public class poker {

    private static final int HANDSIZE = 5;

    private static final int ROYALFLUSH = 10;
    private static final int STRAIGHTFLUSH = 9;
    private static final int FOUROFAKIND = 8;
    private static final int FULLHOUSE = 7;
    private static final int FLUSH = 6;
    private static final int STRAIGHT = 5;
    private static final int THREEOFAKIND = 4;
    private static final int TWOPAIRS = 3;
    private static final int PAIR = 2;
    private static final int HIGHERNUMBER = 1;

    public String[] firstHand, secondHand;
    // public int[] tmpCardNumber;

    public poker() {
        // initialise hands
        firstHand = new String[5];
        secondHand = new String[5];
        // tmpCardNumber = new int[5];
    }

    public void initialiseHands(String[] cards) {
        for (int i = 0; i < cards.length; i++) {
            if (i < HANDSIZE) {
                firstHand[i] = cards[i];
            } else {
                secondHand[i - HANDSIZE] = cards[i];
            }
        }
        // 2, 3, 4, 5, 6, 7, 8, 9, A, J, K, Q, T
        Arrays.sort(firstHand);
        Arrays.sort(secondHand);
    }

    public int checkHands(String[] hand) {

        if (isRoyalFlush(hand)) {
            return 10;
        } else if (isStraightFlush(hand)) {
            return 9;
        } else if (isFourOfaKind(hand)) {
            return 8;
        } else if (isFullHouse(hand)) {
            return 7;
        } else if (isFlush(hand)) {
            return 6;
        } else if (isStraight(hand)) {
            return 5;
        } else if (isThreeOfaKind(hand)) {
            return 4;
        } else if (isTwoPairs(hand)) {
            return 3;
        } else if (isPair(hand)) {
            return 2;
        }
        // System.out.println("Higher card");
        return 1;
    }

    public int checkWin(int firstHandRank, int secondHandRank) {
        if (firstHandRank > secondHandRank) {
            return 1;
        } else if (firstHandRank < secondHandRank) {
            return 2;
        } else {
            return checkHighCard(firstHand, secondHand, firstHandRank);
        }
    }

    private int convertCardNumber(char cardNumber) {
        if (Character.getNumericValue(cardNumber) > 9) {
            if (cardNumber == 'T') {
                return 10;
            } else if (cardNumber == 'J') {
                return 11;
            } else if (cardNumber == 'Q') {
                return 12;
            } else if (cardNumber == 'K') {
                return 13;
            } else if (cardNumber == 'A') {
                return 14;
            }
        }
        return Character.getNumericValue(cardNumber);
    }

    private int checkHighCard(String[] fH, String[] sH, int rank) {
        // Pair, Two Pairs, Three of a kind, Full house and Four of a kind need to check
        // the pairs first
        int nFirstHandMax = 0;
        int nSecondHandMax = 0;
        int nFirstHandMin = 15;
        int nSecondHandMin = 15;
        HashMap<Integer, Integer> firstCardInfo = null;
        HashMap<Integer, Integer> secondCardInfo = null;

        if (rank == STRAIGHT || rank == FLUSH || rank == STRAIGHTFLUSH || rank == ROYALFLUSH) {
            // Higher number win
            for (int i = 0; i < HANDSIZE; i++) {

                if (nFirstHandMax < convertCardNumber(fH[i].charAt(0))) {
                    nFirstHandMax = convertCardNumber(fH[i].charAt(0));
                }
                if (nSecondHandMax < convertCardNumber(sH[i].charAt(0))) {
                    nSecondHandMax = convertCardNumber(sH[i].charAt(0));
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else {
                return 2;
            }
        } else if (rank == FULLHOUSE) {
            // Check higher pair cards
            firstCardInfo = checkSameCardNumberUnderThree(fH);
            secondCardInfo = checkSameCardNumberUnderThree(sH);

            Set<Entry<Integer, Integer>> firstEntrySet = firstCardInfo.entrySet();
            Set<Entry<Integer, Integer>> secondEntrySet = secondCardInfo.entrySet();
            // Entry<Integer, Integer> maxEntry = null;

            for (Entry<Integer, Integer> entry : firstEntrySet) {
                if (entry.getValue() == 3) {
                    nFirstHandMax = entry.getKey();
                }
            }

            for (Entry<Integer, Integer> entry : secondEntrySet) {
                if (entry.getValue() == 3) {
                    nSecondHandMax = entry.getKey();
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else if (nFirstHandMax < nSecondHandMax) {
                return 2;
            } else if (nFirstHandMax == nSecondHandMax) {
                for (Entry<Integer, Integer> entry : firstEntrySet) {
                    if (entry.getValue() == 2) {
                        nFirstHandMax = entry.getKey();
                    }
                }

                for (Entry<Integer, Integer> entry : secondEntrySet) {
                    if (entry.getValue() == 2) {
                        nSecondHandMax = entry.getKey();
                    }
                }

                if (nFirstHandMax > nSecondHandMax) {
                    return 1;
                } else {
                    return 2;
                }
            }
        } else if (rank == FOUROFAKIND) {
            // Check higher pair cards
            firstCardInfo = checkSameCardNumberUnderThree(fH);
            secondCardInfo = checkSameCardNumberUnderThree(sH);

            Set<Entry<Integer, Integer>> firstEntrySet = firstCardInfo.entrySet();
            Set<Entry<Integer, Integer>> secondEntrySet = secondCardInfo.entrySet();
            // Entry<Integer, Integer> maxEntry = null;

            for (Entry<Integer, Integer> entry : firstEntrySet) {
                if (entry.getValue() == 4) {
                    nFirstHandMax = entry.getKey();
                }
            }

            for (Entry<Integer, Integer> entry : secondEntrySet) {
                if (entry.getValue() == 4) {
                    nSecondHandMax = entry.getKey();
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else {
                return 2;
            }

        } else if (rank == THREEOFAKIND) {
            // Check higher pair cards

            firstCardInfo = checkSameCardNumberUnderThree(fH);
            secondCardInfo = checkSameCardNumberUnderThree(sH);

            Set<Entry<Integer, Integer>> firstEntrySet = firstCardInfo.entrySet();
            Set<Entry<Integer, Integer>> secondEntrySet = secondCardInfo.entrySet();
            // Entry<Integer, Integer> maxEntry = null;

            for (Entry<Integer, Integer> entry : firstEntrySet) {
                if (entry.getValue() == 3) {
                    nFirstHandMax = entry.getKey();
                }
            }

            for (Entry<Integer, Integer> entry : secondEntrySet) {
                if (entry.getValue() == 3) {
                    nSecondHandMax = entry.getKey();
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else {
                return 2;
            }
        } else if (rank == TWOPAIRS) {
            // Check higher pair cards

            firstCardInfo = checkSameCardNumberUnderThree(fH);
            secondCardInfo = checkSameCardNumberUnderThree(sH);

            Set<Entry<Integer, Integer>> firstEntrySet = firstCardInfo.entrySet();
            Set<Entry<Integer, Integer>> secondEntrySet = secondCardInfo.entrySet();
            // Entry<Integer, Integer> maxEntry = null;

            for (Entry<Integer, Integer> entry : firstEntrySet) {
                if (entry.getValue() == 2) {
                    if (nFirstHandMax < entry.getKey()) {
                        nFirstHandMax = entry.getKey();
                    }
                }
            }

            for (Entry<Integer, Integer> entry : secondEntrySet) {
                if (entry.getValue() == 2) {
                    if (nSecondHandMax < entry.getKey()) {
                        nSecondHandMax = entry.getKey();
                    }
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else if (nFirstHandMax < nSecondHandMax) {
                return 2;
            } else if (nFirstHandMax == nSecondHandMax) {
                for (Entry<Integer, Integer> entry : firstEntrySet) {
                    if (entry.getValue() == 2) {
                        if (nFirstHandMin > entry.getKey()) {
                            nFirstHandMin = entry.getKey();
                        }
                    }
                }

                for (Entry<Integer, Integer> entry : secondEntrySet) {
                    if (entry.getValue() == 2) {
                        if (nSecondHandMin < entry.getKey()) {
                            nSecondHandMin = entry.getKey();
                        }
                    }
                }

                if (nFirstHandMin > nSecondHandMin) {
                    return 1;
                } else if (nFirstHandMin < nSecondHandMin) {
                    return 2;
                } else if (nFirstHandMin == nSecondHandMin) {
                    for (Entry<Integer, Integer> entry : firstEntrySet) {
                        if (entry.getValue() == 1) {
                            nFirstHandMax = entry.getKey();
                        }
                    }

                    for (Entry<Integer, Integer> entry : secondEntrySet) {
                        if (entry.getValue() == 1) {
                            nSecondHandMax = entry.getKey();
                        }
                    }

                    if (nFirstHandMax > nSecondHandMax) {
                        return 1;
                    } else {
                        return 2;
                    }
                }
            }
        } else if (rank == PAIR) {
            // Check higher pair cards
            firstCardInfo = checkSameCardNumberUnderThree(fH);
            secondCardInfo = checkSameCardNumberUnderThree(sH);

            Set<Entry<Integer, Integer>> firstEntrySet = firstCardInfo.entrySet();
            Set<Entry<Integer, Integer>> secondEntrySet = secondCardInfo.entrySet();
            // Entry<Integer, Integer> maxEntry = null;

            for (Entry<Integer, Integer> entry : firstEntrySet) {
                if (entry.getValue() == 2) {
                    if (nFirstHandMax < entry.getKey()) {
                        nFirstHandMax = entry.getKey();
                    }
                }
            }

            for (Entry<Integer, Integer> entry : secondEntrySet) {
                if (entry.getValue() == 2) {
                    if (nSecondHandMax < entry.getKey()) {
                        nSecondHandMax = entry.getKey();
                    }
                }
            }

            if (nFirstHandMax > nSecondHandMax) {
                return 1;
            } else if (nFirstHandMax < nSecondHandMax) {
                return 2;
            } else if (nFirstHandMax == nSecondHandMax) {
                for (Entry<Integer, Integer> entry : firstEntrySet) {
                    if (entry.getValue() == 1) {
                        nFirstHandMax = entry.getKey();
                    }
                }

                for (Entry<Integer, Integer> entry : secondEntrySet) {
                    if (entry.getValue() == 1) {
                        nSecondHandMax = entry.getKey();
                    }
                }

                if (nFirstHandMax > nSecondHandMax) {
                    return 1;
                } else {
                    return 2;
                }
            }
        } else if (rank == HIGHERNUMBER) {
            // Check higher pair cards
            int[] firstCardNumbers = new int[5];
            int[] secondCardNumbers = new int[5];

            for (int i = 0; i < HANDSIZE; i++) {
                firstCardNumbers[i] = convertCardNumber(fH[i].charAt(0));
                secondCardNumbers[i] = convertCardNumber(sH[i].charAt(0));
            }

            Arrays.sort(firstCardNumbers);
            Arrays.sort(secondCardNumbers);

            for (int j = HANDSIZE - 1; j >= 0; j--) {
                if (firstCardNumbers[j] > secondCardNumbers[j]) {
                    return 1;
                } else if (firstCardNumbers[j] < secondCardNumbers[j]) {
                    return 2;
                }
            }
            System.out.println("Error" + Arrays.toString(fH) + " and " + Arrays.toString(sH));
            return -1;
        }

        return 0;
    }

    private boolean checkSameSuit(String[] hand) {
        char suit = hand[0].charAt(1);
        for (int i = 1; i < HANDSIZE - 1; i++) {
            if (suit != hand[i].charAt(1)) {
                return false;
            }
        }
        return true;
    }

    private int[] checkSameCardNumber(String[] hand) {
        char firstCard = hand[0].charAt(0);
        char lastCard = hand[4].charAt(0);

        int sumFirstCardCount = 1;
        int sumLastCardCount = 1;
        int[] results = new int[2];

        for (int i = 1; i < HANDSIZE - 1; i++) {
            if (hand[i].charAt(0) == firstCard) {
                sumFirstCardCount++;
            } else if (hand[i].charAt(0) == lastCard) {
                sumLastCardCount++;
            }
        }
        results[0] = sumFirstCardCount;
        results[1] = sumLastCardCount;
        return results;
    }

    private HashMap<Integer, Integer> checkSameCardNumberUnderThree(String[] hand) {
        HashMap<Integer, Integer> map = new HashMap<>();
        int tmpNumber = 0;

        for (int i = 0; i < HANDSIZE; i++) {
            tmpNumber = convertCardNumber(hand[i].charAt(0));
            if (map.get(tmpNumber) == null) {
                map.put(tmpNumber, 1);
            } else {
                map.put(tmpNumber, map.get(tmpNumber) + 1);
            }
        }

        return map;
    }

    private boolean isRoyalFlush(String[] hand) {
        // Ten, Jack, Queen, King and Ace in the same suit
        // 2, 3, 4, 5, 6, 7, 8, 9, A, J, K, Q, T
        if (checkSameSuit(hand)) {
            if (hand[0].charAt(0) == 'A') {
                if (hand[1].charAt(0) == 'J') {
                    if (hand[2].charAt(0) == 'K') {
                        if (hand[3].charAt(0) == 'Q') {
                            if (hand[4].charAt(0) == 'T') {
                                // System.out.println("RoyalFlush");
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isStraightFlush(String[] hand) {
        // All five cards in consecutive value order, with the same suit
        if (checkSameSuit(hand)) {
            if (isStraight(hand)) {
                // System.out.println("Straight Flush");
                return true;
            }
        }
        return false;
    }

    private boolean isFourOfaKind(String[] hand) {
        // Four cards of the same value
        int[] results = checkSameCardNumber(hand);
        if (results[0] == 4 || results[1] == 4) {
            // System.out.println("Four Of a kind");
            return true;
        }
        return false;
    }

    private boolean isFullHouse(String[] hand) {
        // Three of a kind and a Pair
        int[] results = checkSameCardNumber(hand);
        if (results[0] == 3 && results[1] == 2 || results[0] == 2 && results[1] == 3) {
            // System.out.println("FullHouse");
            return true;
        }
        return false;
    }

    private boolean isFlush(String[] hand) {
        // All five cards having the same suit
        if (checkSameSuit(hand)) {
            // System.out.println("Flush");
            return true;
        }
        return false;
    }

    private boolean isStraight(String[] hand) {
        // All five cards in consecutive value order
        // 2, 3, 4, 5, 6, 7, 8, 9, A, J, K, Q, T
        int[] cardNumbers = new int[5];
        for (int i = 0; i < HANDSIZE; i++) {
            cardNumbers[i] = convertCardNumber(hand[i].charAt(0));
        }

        Arrays.sort(cardNumbers);

        for (int j = 0; j < HANDSIZE - 1; j++) {
            if ((cardNumbers[j + 1] - cardNumbers[j]) != 1) {
                return false;
            }
        }
        return true;
    }

    private boolean isThreeOfaKind(String[] hand) {
        // Three cards of the same value
        HashMap<Integer, Integer> map = checkSameCardNumberUnderThree(hand);

        Set<Entry<Integer, Integer>> entrySet = map.entrySet();
        Entry<Integer, Integer> maxEntry = null;

        for (Entry<Integer, Integer> entry : entrySet) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }

        int nCard = maxEntry.getValue();

        if (nCard == 3) {
            // System.out.println("Three of a Kind");
            return true;
        }

        return false;
    }

    private boolean isTwoPairs(String[] hand) {
        // Two different pairs
        HashMap<Integer, Integer> map = checkSameCardNumberUnderThree(hand);

        Set<Entry<Integer, Integer>> entrySet = map.entrySet();
        int nCount = 0;

        for (Entry<Integer, Integer> entry : entrySet) {
            if (entry.getValue() == 2) {
                nCount++;
            }
        }

        if (nCount == 2) {
            // System.out.println("Two Pairs");
            return true;
        }

        return false;
    }

    private boolean isPair(String[] hand) {
        // Two cards of same value
        HashMap<Integer, Integer> map = checkSameCardNumberUnderThree(hand);

        Set<Entry<Integer, Integer>> entrySet = map.entrySet();
        Entry<Integer, Integer> maxEntry = null;

        int nCount = 0;

        for (Entry<Integer, Integer> entry : entrySet) {
            if (entry.getValue() == 2) {
                if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                    maxEntry = entry;
                }
                nCount++;
            }
        }

        if (nCount == 1) {
            // System.out.println("Pair");
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        String rawCardData;
        String[] cards;
        Scanner scn = new Scanner(System.in);

        int nWinner = 0;
        int nFirstHandWin = 0;
        int nSecondHandWin = 0;
        int nFirstHandRank = -1;
        int nSecondHandRank = -1;

        while (scn.hasNext()) {
            rawCardData = scn.nextLine();
            cards = rawCardData.split("\\s+");

            poker Pocker = new poker();
            Pocker.initialiseHands(cards);

            // System.out.println("Hand 1 : " + Arrays.toString(Pocker.firstHand));
            // System.out.println("Hand 2 : " + Arrays.toString(Pocker.secondHand));

            nFirstHandRank = Pocker.checkHands(Pocker.firstHand);
            nSecondHandRank = Pocker.checkHands(Pocker.secondHand);

            nWinner = Pocker.checkWin(nFirstHandRank, nSecondHandRank);
            if (nWinner == 1) {
                nFirstHandWin++;
            } else if (nWinner == 2) {
                nSecondHandWin++;
            }
        }
        scn.close();
        // String rawCardData = System.console().readLine();
        System.out.println("Player 1: " + nFirstHandWin + " hands");
        System.out.println("Player 2: " + nSecondHandWin + " hands");
    }
}